from wordcloud import WordCloud
import pandas as pd


def create(text):
    wordcloud = WordCloud(max_font_size=50, max_words=100,
                          background_color="white").generate(text)

    return wordcloud


def count(text):
    text = clean(text)

    words = text.split(' ')

    word_frequency = {}

    for i in words:

        if i in word_frequency:
            word_frequency[i] += 1

        else:
            word_frequency[i] = 1

    return word_frequency


def clean(text):
    # filter out stop words ... just append the list
    blacklist = ['Lorem', 'in']

    words = text.split()

    filtered_words = [word for word in words if word not in blacklist]

    return ' '.join(filtered_words)


def to_df(word_count: dict):
    df = pd.DataFrame.from_dict(word_count)
