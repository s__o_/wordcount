import docx


def read(doc_file):
    return docx.Document(doc_file)


def get_full_text(doc):

    fullText = []
    for paragraph in doc.paragraphs:
        fullText.append(paragraph.text)

    return ''.join(fullText)
