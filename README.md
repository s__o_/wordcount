# WordCounter

Create your own word cloud from any Word file!

![example word cloud](test.png)

## Installation

To install this package, follow the steps below:

```bash
pip install git+https://gitlab.com/s__o_/wordcount.git
```

## Run

Run the following command to create a word cloud (and CSV file containing information about the word's frequency) from a Word.docx file:

```bash
python run.py <word_file.docx>
```

For example:
```bash
python run.py test.docx
```

## Configuration

There is not much to configure yet ... except a blacklist containing words that should be filter out prior the analysis.

To configure the list go to `/wcloud/viz/cloud.py` and extend the `blacklist`-element located in the `clean()` function.

For example:

```python
blacklist = ['Lorem', 'in']
```
 
 This removes the two terms "Lorem" and "in" from the text corpus.