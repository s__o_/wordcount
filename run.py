import pandas as pd
import sys

from wcloud.viz import cloud
from wcloud.io import (
    cloud as chelper,
    word as whelper
)


def main(argv):

    file_name = argv[0].split('.docx')[0]

    # read Word file and extract text
    doc = whelper.read(argv[0])
    text = whelper.get_full_text(doc)

    # clean text (e.g., remove word)
    text = cloud.clean(text)

    # create word cloud
    w = cloud.create(text)

    # count word frequency
    counter = cloud.count(text)

    df = pd.DataFrame.from_dict(
        {'word': list(counter.keys()), 'frequency': list(counter.values())})

    # save word frequency as CSV file and export word cloud to picture
    df.to_csv(file_name + '.csv')
    chelper.export(w, file_name + '.png')


if __name__ == "__main__":
    main(sys.argv[1:])
