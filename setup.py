from setuptools import setup, find_packages  # type: ignore


requirements = [
    'wordcloud',
    'pandas',
    'python-docx'
]


setup(
    name='Word2Cloud',
    version='0.1',
    description='Create a word cloud from a word document',
    url='https://gitlab.hpi.de/simon.remy/',
    author='Simon Remy',
    author_email='kontakt@sremy.de',
    keywords='word cloud worl cloud',
    packages=find_packages(),
    install_requires=requirements,
    include_package_data=True,
)
